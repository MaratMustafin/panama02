<?php require('partials/header.php'); ?>
<header id="header">
				<div class="logo"><a href="index.html">Transitive <span>by TEMPLATED</span></a></div>
				<a href="#menu" class="toggle"><span>Menu</span></a>
</header>
<?php require('partials/nav.php'); ?>

		<!-- One -->
        <section id="one" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="image fit">
							<img src="images/pic01.jpg" alt="" />
						</div>
						<div class="content">
							<header class="align-center">
								<h2>Статьи</h2>
								<p>статьи, которые оставили пользователи</p>
							</header>
                            <ul>
                            <?php foreach ($articles as $article) : ?>
                                <li>Номер статьи:<?= $article->id; ?></li>
                                <li>Имя:<?= $article->name; ?></li>
                                <li>Почта:<?= $article->email; ?></li>
                                <li>Категория работника:<?= $article->dept; ?></li>
                                <li>Статья:<?= $article->message; ?></li>     
                                <hr>
                            <?php endforeach; ?>                       
                            </ul>
							
						</div>
				</div>
			</section>

		<!-- Four -->
			<section id="four" class="wrapper style3">
				<div class="inner">

					<header class="align-center">
						<h2>Morbi interdum mollis sapien</h2>
						<p>Cras aliquet urna ut sapien tincidunt, quis malesuada elit facilisis. Vestibulum sit amet tortor velit. Nam elementum nibh a libero pharetra elementum. Maecenas feugiat ex purus, quis volutpat lacus placerat malesuada. Praesent in sem ex. Morbi mattis sapien pretium tellus venenatis, at egestas urna ornare.</p>
					</header>

				</div>
			</section>

		<!-- Footer -->
			<footer id="footer" class="wrapper">
				<div class="inner">
					<section>
						<div class="box">
							<div class="content">Оставьте отзыв о сайте.</h2>
								<hr />
								<form action="/post" method="post">
									<div class="field half first">
										<label for="name">Ваше имя</label>
										<input name="name" id="name" type="text" placeholder="Имя">
									</div>
									<div class="field half">
										<label for="email">Ваша почта</label>
										<input name="email" id="email" type="email" placeholder="Почта">
									</div>
									<div class="field">
										<label for="dept">Ваша специализация</label>
										<div class="select-wrapper">
											<select name="dept" id="dept">
												<option value="">- Категория -</option>
												<option value="1">Работник 1</option>
												<option value="1">Работник 2</option>
												<option value="1">Работник 3</option>
												<option value="1">Работник 4</option>
											</select>
										</div>
									</div>
									<div class="field">
										<label for="message">Сообщение</label>
										<textarea name="message" id="message" rows="6" placeholder="Введите сообщение"></textarea>
									</div>
									<ul class="actions align-center">
										<li><input value="Send Message" class="button special" type="submit"></li>
									</ul>
								</form>
							</div>
						</div>
					</section>
					<div class="copyright">
						&copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images <a href="https://unsplash.com/">Unsplash</a> Video <a href="http://coverr.co/">Coverr</a>.
					</div>
				</div>
			</footer>

<?php require('partials/footer.php'); ?>