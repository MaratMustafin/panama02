<?php

namespace App\Controllers;

use App\Core\App;

class ArticleController 
{
    public function index() 
    {
        $language = "PHP";
        $title = "Статьи про ".$language;

        $articles = App::get('database')->selectAll('articles');

        return view('articles',[
            'title' => $title,
            'articles' => $articles
        ]);
    }
    public function post() 
    {
        App::get('database')->insert('articles',[
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'dept' => $_POST['dept'],
            'message' => $_POST['message']
        ]);
        return redirect('articles');
    }
}