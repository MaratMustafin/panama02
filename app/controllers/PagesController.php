<?php

namespace App\Controllers;

class PagesController 
{
    public function index() 
    {
        $title = "Главная страница";
        return view('index',[
            'title' => $title
        ]);
    }
}